<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AttrVal;
use AppBundle\Entity\CheckData;
use AppBundle\Entity\DataSetY;
use AppBundle\Entity\Interval;
use AppBundle\Entity\NumberOfIntervals;
use AppBundle\Entity\Rule;
use AppBundle\Entity\TextBox;
use AppBundle\Entity\TNode;
use AppBundle\Form\UploadCSVType;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    const DIR = '/../web/upload/';

    /** @var DataSetY  */
    private $dsy;

    /** @var  TNode */
    private $node;

    private $tableView = [];


    public function __construct(){
        $this->textListIntervalName = new ArrayCollection();
        $this->textListIntervalValue = new ArrayCollection();
        $this->rules = new ArrayCollection();
        $this->comboList = new ArrayCollection();
        $this->textNumeric = new ArrayCollection();
        $this->dsy = new DataSetY();
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $file = $request->request->get('fileName');
        $generate = $request->request->get('generate');
        $check = $request->request->get('check');
        $checkRule = $request->request->get('check_rule');
        $deleteFile = $request->request->get('deleteFile');
        $intervalAccept = $request->request->get('interval_accept');
        $inputs = $request->request->get('input');
        $checkResult = $checkRuleResult = null;


        $form = $this->createForm(UploadCSVType::class, null, ['method' => 'POST']);

        $upload = $this->uploadFile($request, $form);
        if($upload != null)
            $file = $upload;

        $files = $this->scanFiles();
        $this->readFile($file, $files, $inputs);

        if($intervalAccept){
            $this->getIntervalsFromInputs($request->request->get('interval'));
            $generate = true;
        }
        if($check || $checkRule)
            $generate = true;

        $distinctValues = $this->dsy->getDistinctValuesToSelect();

        if($generate)
            $this->buildRun();

        if($check || $checkRule){
            $this->getRulesFromInputs($request->request->get('rule'), $distinctValues);
        }

        if($check)
            $checkResult = $this->checkButton();

        if($checkRule)
            $checkRuleResult = $this->checkRuleButton();

        $json = (isset($this->tableView)) ? json_encode($this->tableView) : null;

        return $this->render('AppBundle:Default:index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'files' => $files,
            'fileSelect' => $file,

            'headers' => $this->dsy->getHeaders(),
            'data' => $this->dsy->getData(),
            'countColumn' => $this->dsy->getNumberOfAttr()+1,
            'countRow' => $this->dsy->getNumbersOfRows(),
            'tableJson' => $json,
            'numberOfIntervals' => $this->textListIntervalValue,
            'rulesList' => $this->rules,
            'distinctValues' => $distinctValues,
            'generateClick' => $generate,
            'checkResult' => $checkResult,
            'checkRuleResult' => $checkRuleResult
        ]);
    }

    public function buildRun(){
        $intervalList = $this->getNumberOfIntervals();

        $this->dsy->tmpIntervals($intervalList);
        $this->buildTree($this->dsy);
    }



    public function count(TNode $parent = null, string $attributeIn = null, string $attributeInValue = null, bool $isNumeric = null, Interval $interval = null, DataSetY $ds = null, bool $stop = false, string $resultX = ""){


        if($ds->getNumbersOfRows() == 0){
            TNode::createNode($attributeIn, $attributeInValue, "", "", $resultX, "", $parent, $isNumeric, $interval);
            return;
        }

        if($ds->getNumberOfAttr() == 0){
            $result = $ds->getMaxResults();
            TNode::createNode($attributeIn, $attributeInValue, "", "", $result, "", $parent, $isNumeric, $interval);
            return;
        }

        if($ds->getNumberOfResults() == 1){
            $result = $ds->getMaxResults();
            TNode::createNode($attributeIn, $attributeInValue, "", "", $result, "", $parent, $isNumeric, $interval);
            return;
        }


        if($stop){
            $result = $resultX;
            TNode::createNode($attributeIn, $attributeInValue, "", "", $result, "", $parent, $isNumeric, $interval);
        }

        $h = $ds->countEntropy();
        $bestAttr = -1;
        $bestIG = -1;
        $bestHw = 0;

        for($i = 0; $i < $ds->getNumberOfAttr(); $i++){

            $hw = $ds->countAttrEntropy($i);
            $ig = $h - $hw;

            if($ig > $bestIG){
                $bestIG = $ig;
                $bestAttr = $i;
                $bestHw = $hw;
            }
        }

        $displayName = $ds->getOriginName($ds->getAttrName($bestAttr));

        $node = TNode::createNode($attributeIn, $attributeInValue, $ds->getAttrName($bestAttr), $displayName, "", $bestIG, $parent, $isNumeric, $interval);
        $node->setH($h);
        $node->setHw($bestHw);

        $list = $ds->getAttrValuesFull($bestAttr);


        for($i = 0; $i< $list->count(); $i++){
            if($list->get($i)->getCount() == 0){

                $ds2 = $ds->getSubSet($ds->getAttrName($bestAttr), $list->get($i)->getName(), $list->get($i)->isNumeric(), $list->get($i)->getInterval());

                if($ds2->getNumbersOfRows() == 0){
                    $asd = 0;
                    $asd++;
                }

                $result = $ds->getMaxResults();

                $this->count($node, $ds->getAttrName($bestAttr), $list->get($i)->getName(), $list->get($i)->isNumeric(), $list->get($i)->getInterval(), $ds2, true, $result);

                $a = 0;
                $a++;
            }else{
                $ds2 = $ds->getSubSet($ds->getAttrName($bestAttr), $list->get($i)->getName(), $list->get($i)->isNumeric(), $list->get($i)->getInterval());

                if($ds2->getNumbersOfRows() == 0){
                    $a = 0;
                    $a++;
                }

                $this->count($node, $ds->getAttrName($bestAttr), $list->get($i)->getName(), $list->get($i)->isNumeric(), $list->get($i)->getInterval(), $ds2);
            }
        }



    }

    public function buildTree(DataSetY $ds){

        $this->node = new TNode("", "", "", "", "", null, false, null);

        $this->count($this->node, "", "", false, null, $ds);

       //wyświetlenie drzewa
        $this->generateDisplayView($this->node->getChildren()->get(0),$this->tableView);


        $rule = new Rule();
        $this->buildRules($this->node, $rule);

        for($i = 0; $i < $this->rules->count(); $i++){
            /** @var Rule $ruleSelect */
            $this->dsy->countSupport($this->rules->get($i));
            $this->dsy->countConfidence($this->rules->get($i));
        }

    }

    /** @var  ArrayCollection */
    private $rules;

    /**
     * @param TNode $node
     * @param Rule $rule
     */
    public function buildRules(TNode $node, Rule $rule){

        if($node->getChildren()->count() > 0){
            for($i = 0; $i < $node->getChildren()->count(); $i++){

                $av = new AttrVal();
                $av->setAttribute($node->getAttribute());
                /** @var TNode $child */
                $child = $node->getChildren()->get($i);
                $av->setValue($child->getAttributeInValue());

                if($child->isIsNumeric()){
                    $av->setInterval($child->getInterval());
                    $av->setIsNumeric(true);
                }

                $newRule = $rule->getCopy();

                if(strlen($av->getAttribute()) > 0){
                    $newRule->getVal()->add($av);
                }

                $this->buildRules($child, $newRule);

            }
        }else{
            $rule->setResult($node->getResult());
            $this->rules->add($rule);
        }

    }

    private function readFile($filename, $files, $inputs = null){
        $dataSetY= new DataSetY();
        if($filename == null && sizeof($files) == 0)
            return null;

        if($filename == null)
            $filename = $files[0];

        $this->dsy = $dataSetY->read($this->get('kernel')->getRootDir().self::DIR.$filename, $inputs);

        $this->addNumOfIntervals($this->dsy);

    }

    /** @var  ArrayCollection */
    private $textListIntervalName;

    /** @var ArrayCollection  */
    private $textListIntervalValue;

    /**
     * @return ArrayCollection
     */
    private function getNumberOfIntervals(){
        $list = new ArrayCollection();
        for($i = 0; $i < $this->textListIntervalValue->count(); $i++){

            $sNumber = $this->textListIntervalValue->get($i)->getText();

            if(is_numeric($sNumber) == false){
                $number = 5;
            }else{
                $number = $sNumber;
            }

            $noi = new NumberOfIntervals();
            $noi->setNumberOfIntervals($number);
            $noi->setAttributeName($this->textListIntervalValue->get($i)->getTag());
            $list->add($noi);
        }

        return $list;
    }

    /**
     * @param DataSetY $ds
     */
    private function addNumOfIntervals(DataSetY $ds){

        for($i = 0; $i < $ds->getNumberOfAttr(); $i++){
            $attrName = $ds->getAttrName($i);
            $attrNameDis = $ds->getOriginName($i);

            if($ds->getNumeric($i)){

                $textBox = new TextBox();
                $textBox->setText($attrNameDis);
                $textBox->setTag($attrName);
                $textBox->setVisible(true);

                $this->textListIntervalName->add($textBox);

                $textBoxNum = new TextBox();
                $textBoxNum->setVisible(true);
                $textBoxNum->setText(5);
                $textBoxNum->setTag($attrName);

                $this->textListIntervalValue->add($textBoxNum);
            }
        }
    }

    /** @var  ArrayCollection */
    private $comboList;

    /** @var  ArrayCollection */
    private $textNumeric;

    /**
     * @return CheckData
     */
    private function buildQuery(){
        $cd = new CheckData();

        for($i = 0; $i < $this->comboList->count(); $i++){
            $av = new AttrVal();
            $av->setIsNumeric(false);
            $av->setAttribute($this->comboList->get($i)->getTag());
            $av->setValue($this->comboList->get($i)->getText());
            $cd->getAttributes()->add($av);
        }

        for($i = 0; $i < $this->textNumeric->count(); $i++){
            $av = new AttrVal();
            $av->setIsNumeric(true);
            $av->setAttribute($this->textNumeric->get($i)->getTag());


            if(!is_numeric($this->textNumeric->get($i)->getText())){
                $val = 0;
            }else{
                $val = $this->textNumeric->get($i)->getText();
            }

            $av->setRvalue($val);
            $av->setValue('');
            $cd->getAttributes()->add($av);
        }

        return $cd;

    }

    /**
     * @return string
     */
    private function checkButton(){

        $cd = $this->buildQuery();
        $cd->setResult('');
        $this->node->check($cd);

        return $cd->getResult();
    }


    /**
     * @param CheckData $cd
     * @return string
     */
    private function checkRules(CheckData $cd){

        for($i = 0; $i < $this->rules->count(); $i++){

            if($this->rules->get($i)->check($cd)){

                $return['result'] = $this->rules->get($i)->getResult();
                $return['rule'] = $this->rules->get($i)->__toString();
                $return['RuleID'] = $i;

                return $return;
            }
        }

        return '?';
    }

    /**
     * @return string
     */
    private function checkRuleButton(){
        $cd = $this->buildQuery();

        return $this->checkRules($cd);
    }











    /**
     * @param Request $request
     * @param Form $form
     * @return null
     */
    private function uploadFile($request, $form){
        $file = null;


        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $fileUpload = $form->getData()['file'];

            $fileName= $fileUpload->getClientOriginalName();

            $fileUpload->move(
                $this->get('kernel')->getRootDir().self::DIR,
                $fileName
            );

            $file = $fileName;
        }

        return $file;
    }


    /**
     * @return array
     */
    private  function scanFiles(){
        $path = $this->get('kernel')->getRootDir().self::DIR;

        $returnArray = [];

        foreach(scandir($path) as $file){
            if(!in_array($file, ['.', '..']) && !is_dir($path.DIRECTORY_SEPARATOR.$file) && $file != '.DS_Store')
                $returnArray[] = $file;
        }

        return $returnArray;
    }


    /**
     * @param TNode $node
     * @param $tableView
     */
    private function generateDisplayView(TNode $node, &$tableView){

        $tableView = [];
        if($node->isIsNumeric())
            $tableView['text']['name'] = 'Wartość: '.$node->getName();
        if ($node->getAttributeInValue() != null)
            $tableView['text']['name'] = 'Wartość:'.$node->getAttributeInValue();
        if ($node->getAttributeDisplayName() != null)
            $tableView['text']['title'] = 'Najlepszy atrybut: ' . $node->getAttributeDisplayName();
        if ($node->getH() != null)
            $tableView['text']['contact'] = 'Entropy: ' . number_format($node->getH(), 2, ',', ' ').' ';
        else
            $tableView['text']['contact'] = 'Entropy: 0.00 ';
        if ($node->getHw() != null)
            $tableView['text']['contact'] .= 'Hw: '.number_format($node->getHw(), 2, ',', ' '). '  ';

        if ($node->getInfo() != null)
                $tableView['text']['contact'] .= 'Gain: ' . number_format($node->getInfo(), 2, ',', ' '). '  ';
        if ($node->getAttributeInValue() != null)
            $tableView['text']['label'] = $node->getAttributeInValue();

        if ($node->getAttribute() == null)
            if ($node->getResult() != null)
                $tableView['children'][0]['text']['title'] = 'Decyzja: ' . $node->getResult();
            else
                $tableView['children'][0]['text']['title'] = 'Decyzja: NULL';

        if($node->getChildren()->count() > 0){

            /**
             * @var  $key
             * @var Tnode $nextNode
             */
            foreach($node->getChildren() as $key => $nextNode){

                $tableView['children'][$key] = [];
                $this->generateDisplayView($nextNode, $tableView['children'][$key]);

            }

        }

    }

    private function getIntervalsFromInputs($inputs = null){
        $list = new ArrayCollection();
        for($i = 0; $i < $this->textListIntervalValue->count(); $i++){

            /** @var TextBox $interval */
            $interval = $this->textListIntervalValue->get($i);

            $number = $inputs[$interval->getTag()];

            $newTextBox = new TextBox();
            $newTextBox->setTag($interval->getTag());
            $newTextBox->setText($number);
            $newTextBox->setVisible(false);
            $list->add($newTextBox);
        }

        $this->textListIntervalValue = $list;

    }

    /**
     * @param $inputs
     * @param ArrayCollection $distinctValues
     */
    private function getRulesFromInputs($inputs, $distinctValues){
        for($i = 0; $i < $distinctValues->count(); $i++){

            /** @var TextBox $textBox */
            $textBox = $distinctValues->get($i);

            $textBox->setText($inputs[$textBox->getTag()]);

            if($textBox->isNumeric()){
                $this->textNumeric->add($textBox);
            }else{
                $this->comboList->add($textBox);
            }

        }

    }

}
