<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Interval
 */
class Interval
{
    /** @var bool  */
    private $useLimit1 = false;

    /** @var bool  */
    private $useLimit2 = false;

    /** @var  double */
    private $limit1;

    /** @var  double */
    private $limit2;

    /**
     * @return bool
     */
    public function isUseLimit1(): bool
    {
        return $this->useLimit1;
    }

    /**
     * @param bool $useLimit1
     */
    public function setUseLimit1(bool $useLimit1)
    {
        $this->useLimit1 = $useLimit1;
    }

    /**
     * @return bool
     */
    public function isUseLimit2(): bool
    {
        return $this->useLimit2;
    }

    /**
     * @param bool $useLimit2
     */
    public function setUseLimit2(bool $useLimit2)
    {
        $this->useLimit2 = $useLimit2;
    }

    /**
     * @return float
     */
    public function getLimit1(): float
    {
        return $this->limit1;
    }

    /**
     * @param float $limit1
     */
    public function setLimit1(float $limit1)
    {
        $this->limit1 = $limit1;
    }

    /**
     * @return float
     */
    public function getLimit2(): float
    {
        return $this->limit2;
    }

    /**
     * @param float $limit2
     */
    public function setLimit2(float $limit2)
    {
        $this->limit2 = $limit2;
    }

    /**
     * @return string
     */
    function __toString()
    {
        if($this->useLimit1 && $this->useLimit2)
            return '[' . $this->limit2 . ',' . $this->limit1 . ')';
        else
            if($this->useLimit1)
                return '( - inf ' . $this->limit1 . ')';
            else
                if($this->useLimit2)
                    return '[' . $this->limit2 . ',inf)';

        return '';
    }

    /**
     * @param float $value
     * @return bool
     */
    public function check(float $value){
        if($this->useLimit1){

            if($value >= $this->limit1)
                return false;

        }

        if($this->useLimit2){
            if($value < $this->limit2)
                return false;
        }

        if($this->useLimit1 == true || $this->useLimit2 == true){
            return true;
        }

        return false;
    }

}
