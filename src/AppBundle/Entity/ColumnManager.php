<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ColumnManager
 */
class ColumnManager
{
    /** @var ArrayCollection  */
    public $columns = [];

    public function __construct(){
        /** @var ArrayCollection columns */
        $this->columns = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getColumns(): ArrayCollection
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns)
    {
        $this->columns = $columns;
    }

    /**
     * @return int
     */
    public function getNumberOfColumns(){
        return sizeof($this->columns);
    }

    /**
     * METODY PRZECIĄŻONE
     */

    /**
     * @param int $id
     * @param string $name
     * @param string $originalName
     */
    private function addColYWithParameters(int $id, string $name, string $originalName){
        $columnY = new ColumnY();
        $columnY->setId($id);
        $columnY->setInterialName($name);
        $columnY->setOriginName($originalName);
        $columnY->setIsNumeric(false);
        $columnY->setResult(false);
        $this->columns->add($columnY);
    }

    /**
     * @param ColumnY $columnY
     */
    private function addColYWithObject(ColumnY $columnY){
        $this->columns->add($columnY);
    }

    /**
     * @param int $id
     * @return string
     */
    private function getOriginNameById(int $id){
        for ($i = 0; $i < sizeof($this->columns); $i++){
            if($this->columns->get($i)->getId() == $id)
                return $this->columns->get($i)->getOriginName();
        }
        return "";
    }

    /**
     * @param string $interialName
     * @return string
     */
    private function getOriginNameByInterialName(string $interialName = null){
        for ($i = 0; $i < sizeof($this->columns); $i++){
            if($this->columns->get($i)->getInterialName() == $interialName)
                return $this->columns->get($i)->getOriginName();
        }
        return "";
    }

    /**
     * @param $name
     * @param $arguments
     * @return string
     */
    public function __call($name, $arguments)
    {
        switch ($name){
            case 'add':
                if(isset($arguments[0]) && is_a($arguments[0], ColumnY::class))
                    $this->addColYWithObject($arguments[0]);
                else
                    $this->addColYWithParameters($arguments[0], $arguments[1], $arguments[2]);
                break;
            case 'getOriginName':
                if(is_numeric($arguments[0]))
                    return $this->getOriginNameById($arguments[0]);
                else
                    return $this->getOriginNameByInterialName($arguments[0]);
                break;
            break;
        }
    }

    /**
     * KONIEC METOD PRZECIĄŻONYCH
     */

    private function getNameById(int $id){
        for ($i = 0; $i < sizeof($this->columns); $i++){
            if($this->columns->get($i)->getId() == $id)
                return $this->columns->get($i)->getInterialName();
        }
        return "";
    }

    /**
     * @param int $id
     * @return int
     */
    public function findColumnById(int $id){

        for($i = 0; $i < sizeof($this->columns); $i++){
            if($this->columns->get($i)->getId() == $id)
                return $i;

        }
        return -1;
    }

    /**
     * @param int $id
     * @param bool $numeric
     */
    public function setIsNumeric(int $id, bool $numeric){
        $index = $this->findColumnById($id);
        $this->columns->get($index)->setIsNumeric($numeric);
    }

    /**
     * @param int $id
     * @param bool $result
     */
    public function setResult(int $id){
        $index = $this->findColumnById($id);
        $this->columns->get($index)->setResult(true);
    }

    /**
     * @param ColumnY $colY
     * @return ColumnY
     */
    private function getClone(ColumnY $colY){
        $columnY = new ColumnY();
        $columnY->setId($colY->getId());
        $columnY->setInterialName($colY->getInterialName());
        $columnY->setIsNumeric($colY->isNumeric());
        $columnY->setOriginName($colY->getOriginName());
        $columnY->setResult($colY->isResult());
        $columnY->setIntervals($colY->getIntervals());

        return $columnY;
    }

    /**
     * @param string $columnName
     * @return ColumnManager
     */
    public function getSubSet(string $columnName = null){
        $columnManager = new ColumnManager();
        $found = false;

        for($i = 0; $i < $this->columns->count(); $i++){
            $columnY = $this->getClone($this->columns->get($i));

            if($found){ // jeśli znaleziono dodaj do managera
                $columnY->setId($columnY->getId()-1);
                $columnManager->add($columnY);
            }else{
                if($columnName == $columnY->getInterialName())
                    $found = true;
                else
                    $columnManager->add($columnY);
            }
        }

        return $columnManager;
    }

    /**
     * @return string
     */
    public function getResultColumnName(){
        for($i = 0; $i < sizeof($this->columns); $i++){
            if($this->columns->get($i)->isResult() == true){
                return $this->columns->get($i)->getInterialName();
            }
        }
        return "";
    }

    /**
     * @return string
     */
    public function getResultColumnNumber(){
        for($i = 0; $i < sizeof($this->columns); $i++){
            if($this->columns->get($i)->isResult() == true){
                return $this->columns->get($i)->getId();
            }
        }
        return "";
    }

    /**
     * @param int $id
     * @return bool
     */
    public function getNumeric(int $id){
        for($i = 0; $i < sizeof($this->columns); $i++){
            if($this->columns->get($i)->getId() == $id){
                return $this->columns->get($i)->isNumeric();
            }
        }
        return false;
    }

    /**
     * @param int $id
     * @return ArrayCollection|null
     */
    public function getIntervals(int $id){
        for($i = 0; $i < sizeof($this->columns); $i++){
            if($this->columns->get($i)->getId() == $id){
                return $this->columns->get($i)->getIntervals();
            }
        }
        return null;
    }

    /**
     * @param int $id
     * @return string
     */
    public function getName(int $id){
        for($i = 0; $i < sizeof($this->columns); $i++){
            if($this->columns->get($i)->getId() == $id)
                return $this->columns->get($i)->getInterialName();
        }
        return "";
    }

    /**
     * @param int $id
     * @param float $min
     * @param float $max
     * @param int $count
     */
    public function buildIntervals(int $id, float $min, float $max, int $count){
        $d = ($max - $min) / floatval($count);

        $intervals = new ArrayCollection();

        $last = $min + $d;

        for($i = 0; $i < $count; $i++){
            if($i == 0){ //POCZĄTEK
                $interval = new Interval();
                $interval->setUseLimit1(true);
                $interval->setUseLimit2(false);

                $interval->setLimit1($last);
                $intervals->add($interval);
            }else{

                if($i == $count - 1){ //KONIEC
                    $interval = new Interval();
                    $interval->setUseLimit1(false);
                    $interval->setUseLimit2(true);

                    $interval->setLimit2($last);
                    $intervals->add($interval);
                }else{ // ŚRODEK
                    $interval = new Interval();
                    $interval->setUseLimit1(true);
                    $interval->setUseLimit2(true);

                    $interval->setLimit2($last);
                    $last = $last + $d;
                    $interval->setLimit1($last);
                    $intervals->add($interval);
                }
            }
        }

        for($i = 0; $i < sizeof($this->columns); $i++){
            if($this->columns->get($i)->getId() == $id){
                $this->columns->get($i)->setIntervals($intervals);
            }
        }

        if($count == 2){

        }else{

        }
    }
}
