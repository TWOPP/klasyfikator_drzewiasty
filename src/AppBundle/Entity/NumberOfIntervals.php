<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NumberOfIntervals
 */
class NumberOfIntervals
{
    /** @var string  */
    private $attributeName = "";

    /** @var int  */
    private $numberOfIntervals;

    /**
     * @return string
     */
    public function getAttributeName(): string
    {
        return $this->attributeName;
    }

    /**
     * @param string $attributeName
     */
    public function setAttributeName(string $attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * @return int
     */
    public function getNumberOfIntervals(): int
    {
        return $this->numberOfIntervals;
    }

    /**
     * @param int $numberOfIntervals
     */
    public function setNumberOfIntervals(int $numberOfIntervals)
    {
        $this->numberOfIntervals = $numberOfIntervals;
    }



}
