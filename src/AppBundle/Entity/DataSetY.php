<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints\File;

/**
 * DataSetY
 */
class DataSetY
{
    /** @var ArrayCollection  */
    private $data;

    /** @var ArrayCollection  */
    private $headers;

    /** @var  ColumnManager */
    private $columnManager;

    /**
     * @return ArrayCollection
     */
    public function getHeaders(): ArrayCollection
    {
        return $this->headers;
    }

    /**
     * @param ArrayCollection $headers
     */
    public function setHeaders(ArrayCollection $headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return ColumnManager
     */
    public function getColumnManager(): ColumnManager
    {
        return $this->columnManager;
    }

    /**
     * @param ColumnManager $columnManager
     */
    public function setColumnManager(ColumnManager $columnManager)
    {
        $this->columnManager = $columnManager;
    }

    /**
     * @return ArrayCollection
     */
    public function getData(): ArrayCollection
    {
        return $this->data;
    }

    /**
     * @param ArrayCollection $data
     */
    public function setData(ArrayCollection $data)
    {
        $this->data = $data;
    }

    public function __construct(){
        $this->data = new ArrayCollection();
        $this->headers = new ArrayCollection();
        $this->columnManager = new ColumnManager();
    }

    /**
     * @param string $file
     * @return DataSetY
     */
    public function read(string $file, $inputs = null){
        ini_set('auto_detect_line_endings', true);
        $data = new DataSetY();

        $file = fopen($file, 'r');

        $line = fgetcsv($file, 10000, ',');
        if($line != null){
            $columns = $line;

            $data->data = new ArrayCollection();

            /**
             * ODCZYT NAGŁÓWKÓW
             */

            for($i = 0; $i < sizeof($columns); $i++){

                $str = $columns[$i];

                $str = str_replace('-', '_', $str);

                    $data->headers->add($str);

                    $colY = new ColumnY();
                    $colY->setId($i);
                    $colY->setOriginName($columns[$i]);
                    $colY->setInterialName($str);
                    $colY->setIsNumeric(false);
                    $colY->setResult(false);

                    $data->columnManager->add($colY); // metoda przeciążona

                    if($i == sizeof($columns) - 1){
                        $data->columnManager->setResult($i);
                    }

                }

                /** @var bool $secondRow */
                $secondRow = true;

            /**
             * ODCZYT WARTOŚCI
             */
                while($line = fgetcsv($file, 10000, ',')){

                    /** @var ArrayCollection $columnData */
                    $columnData = new ArrayCollection();

                    for($i = 0; $i < sizeof($columns); $i++){
                        $d = 0.00;
                        $numberData = false;

                        if(is_numeric($line[$i])){
                            $columnData->add(floatval(trim($line[$i])));
                            $numberData = true;
                        }else{

                            $str2 = trim(str_replace('.', ',', $line[$i]));

                            if(is_numeric($line[$i])) {
                                $columnData->add(floatval(trim($str2)));
                                $numberData = true;
                            }else
                                $columnData->add($line[$i]);
                        }

                        if($secondRow){
                            if($numberData)
                                $data->columnManager->setIsNumeric($i, true);
                            else
                                $data->columnManager->setIsNumeric($i, false);
                        }else{

                        }
                    }
                    $secondRow = false;
                    $data->data->add($columnData);
                }
            }


            if($inputs != null)
            foreach ($inputs as $input) {

                /** @var ArrayCollection $columnData */
                $columnData = new ArrayCollection();
                foreach($input as $val)

                if(is_numeric($val)){
                    $columnData->add(floatval($val));
                }else{
                    $str2 = trim(str_replace('.', ',', $val));

                    if(is_numeric($val)) {
                        $columnData->add(floatval(trim($str2)));
                    }else
                        $columnData->add($val);
                }
                $data->data->add($columnData);
            }
        return $data;
    }


    /**
     * PRACA Z DATA VIEW
     */

    /**
     * @param string|null $column
     * @param string|null $value
     * @param bool|null $numeric
     * @param Interval|null $interval
     * @return DataSetY
     */
    public function getSubSet(string $column = null, string $value = null, bool $numeric = null, Interval $interval = null){
        $subset = new DataSetY();



        if($numeric == false){
            //dv row filter add
            $subset->setData($this->data);
            $subset->setHeaders($this->headers);

            /** @var ArrayCollection $data */
            $subset = $this->removeRowsWithValue($subset,$column, $value);

        }else{

            $finish = false;
            $dtx = $this->data;
            $n = 0;

            do{

                $found = false;
                for($i = $n; $i < $dtx->count(); $i++){

                    $strVal =  $dtx->get($i)->get($this->getAttrId($column));
                    $valueX = floatval($strVal);

                    if($valueX > 0.4 && $valueX < 0.11){
                        $a = 0;
                        $a++;
                    }

                    if($interval->check($valueX) == false){
                        $found = true;
                        $newArray = new ArrayCollection();

                        foreach($dtx as $key => $data){
                            if($i != $key)
                                $newArray->add($data);
                        }
                        $dtx = $newArray;

                        break;
                    }else{
                        $n = $i+1;
                    }

                }

                if(!$found){
                    $finish = true;
                }


            }while(!$finish);

            $subset->setHeaders($this->headers);
            $subset->setData($dtx);

        }

        $subset = $this->removeColumn($subset, $column);

        $subset->setColumnManager($this->columnManager->getSubSet($column));

        return $subset;
    }

    /**
     * @param int $attr
     * @return ArrayCollection
     */
    public function fillResultList(int $attr){
        $counts = new ArrayCollection();
        $resultColumnNumber = $this->columnManager->getResultColumnNumber();

        $isNumeric = $this->columnManager->getNumeric($attr);

        if($isNumeric){
            $intervals = $this->columnManager->getIntervals($attr);

            for($i = 0; $i < $this->data->count(); $i++){
                $strVal = $this->data->get($i)->get($attr);
                $value = floatval($strVal);

                $resultVal = $this->data->get($i)->get($resultColumnNumber);

                $interval = null;

                for($j = 0; $j < $intervals->count(); $j++){
                    if($intervals->get($j)->check($value)){
                        $interval = $intervals->get($j);
                        break;
                    }
                }

                $found = false;

                for($j = 0; $j < $counts->count(); $j++){

                    if($counts->get($j)->getInterval() == $interval){
                        $counts->get($j)->setCount($counts->get($j)->getCount()+1);
                        $counts->get($j)->addValue($resultVal);
                        $found = true;
                        break;
                    }
                }

                if($found == false){
                    $rc = new ResultAttrCountY();
                    $rc->setIsNumeric(true);
                    $rc->setInterval($interval);
                    $rc->setCount(1);
                    $rc->addValue($resultVal);
                    $counts->add($rc);
                }
            }
        }else{
            for($i = 0; $i < $this->data->count(); $i++){

                $value = $this->data->get($i)->get($attr);
                $resultVal = $this->data->get($i)->get($resultColumnNumber);

                $found = false;
                for($j = 0; $j < $counts->count(); $j++){
                    if($counts->get($j)->getName() == $value){
                        $counts->get($j)->setCount($counts->get($j)->getCount() + 1);

                        $counts->get($j)->addValue($resultVal);
                        $found = true;
                        break;
                    }
                }

                if($found == false){
                    $rc = new ResultAttrCountY();
                    $rc->setName($value);
                    $rc->setCount(1);
                    $rc->addValue($resultVal);
                    $counts->add($rc);
                }
            }
        }

        return $counts;
    }

    /**
     * @return ArrayCollection
     */
    private function countResults(){
        $resultColumnNumber = $this->columnManager->getResultColumnNumber();

        $result = new ArrayCollection();

        for($i = 0; $i < $this->data->count(); $i++){


            $name = $this->data->get($i)->get($resultColumnNumber);

            $found = false;

            for($j = 0; $j < $result->count(); $j++){
                if($result->get($j)->getName() == $name){
                    $result->get($j)->setCount($result->get($j)->getCount() + 1);
                    $found = true;
                    break;
                }
            }

            if($found == false){
                $rc = new ResultCount();
                $rc->setName($name);
                $rc->setCount(1);
                $result->add($rc);
            }
        }

        if($result->count() == 0){
            $a = 0;
            $a++;
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getNumberOfResults(){
        $list = $this->countResults();
        return $list->count();
    }

    /**
     * @return mixed
     */
    public function getMaxResults(){
        $result = $this->countResults();

        $bestCount = -1;
        $bestResult = -1;

        for($i = 0; $i < $result->count(); $i++){
            if($result->get($i)->getCount() > $bestCount){
                $bestCount = $result->get($i)->getCount();
                $bestResult = $i;
            }
        }

        return $result->get($bestResult)->getName();
    }

    /**
     * @return int
     */
    public function getNumberOfAttr(){
        return ($this->headers->count() - 1);
    }

    /**
     * @return int
     */
    public function getNumbersOfRows(){
        return $this->data->count();
    }

    /**
     * @return float
     */
    public function countEntropy(){
        $h = 0;
        $result = $this->countResults();

        $rowCount = $this->getNumbersOfRows();

        for($i = 0; $i < $result->count(); $i++){

            $valCount = $result->get($i)->getCount();

            $r = $valCount / $rowCount;

            $hx = -$r * log($r, 2);

            $h = $h + $hx;
        }
        return $h;
    }

    /**
     * @param int $at
     * @return float
     */
    public function countAttrEntropy(int $at){
        $counts = $this->fillResultList($at);

        $h = 0;

        for($i = 0; $i < $counts->count(); $i++){

            $hw = 0;
            $bSum = $counts->get($i)->getSum();

            for($j = 0; $j < $counts->get($i)->getList()->count(); $j++){

                $bValCount = $counts->get($i)->getList()->get($j)->getCount();

                $r = $bValCount / $bSum;

                $hx = -$r * log($r, 2);

                $hw = $hw + $hx;
            }

            $rcount = $this->data->count();
            $vlCount = $counts->get($i)->getCount();

            $rx = $vlCount / $rcount;

            $h = $h + $rx * $hw;
        }

        return $h;
    }

    /**
     * @param int $i
     * @return mixed|null|string
     */
    public function getAttrName(int $i){
        if($i >= $this->headers->count()){
            return ".";
        }else{
            return $this->headers->get($i);
        }
    }

    /**
     * @param int $at
     * @return ArrayCollection
     */
    public function getAttrValues(int $at){
        $counts = $this->fillResultList($at);
        return $counts;
    }

    /**
     * @param int $at
     * @return ArrayCollection
     */
    public function getAttrValuesFull(int $at){
        $counts = $this->fillResultList($at);

        if($this->columnManager->getNumeric($at)){

            $intervals = $this->columnManager->getIntervals($at);

            for($i = 0; $i < $intervals->count(); $i++){

                $found = false;
                for($j = 0; $j < $counts->count(); $j++){

                    if($counts->get($j)->getInterval() == $intervals->get($i)){
                        $found = true;
                        break;
                    }
                }

                if($found == false){
                    $rx = new ResultAttrCountY();
                    $rx->setCount(0);
                    $rx->setInterval($intervals->get($i));
                    $rx->setIsNumeric(true);
                    $counts->add($rx);
                }
            }
        }
//        dump($counts);die();
        return $counts;
    }

    /**
     * @param string $name
     * @return int
     */
    public function getAttrId(string $name = null){
        for($i = 0; $i < $this->headers->count(); $i++){
            if($this->headers->get($i) == $name)
                return $i;
        }
        return -1;
    }

    /**
     * @param Rule $r
     * @return int
     */
    public function countConfidence(Rule $r){
        $confidence = 0;
        $confidence2 = 0;
        $resultColumnName = $this->columnManager->getResultColumnName();

        $resultColumnID = $this->getAttrId($resultColumnName);

        for ($i = 0; $i < $this->data->count(); $i++){

            $ruleOK = true;
            for($j = 0; $j < $r->getVal()->count(); $j++){

                $colID = $this->getAttrId($r->getVal()->get($j)->getAttribute());

                if($this->columnManager->getNumeric($colID)){

                    $strVal = $this->data->get($i)->get($colID);
                    $value = floatval($strVal);

                    if($r->getVal()->get($j)->getInterval()->check($value) == false){
                        $ruleOK = false;
                        break;
                    }
                }else{

                    $value = $this->data->get($i)->get($colID);
                    if($value != $r->getVal()->get($j)->getValue()){
                        $ruleOK = false;
                        break;
                    }
                }
            }

            if($ruleOK){
                $confidence2++;
                $value = $this->data->get($i)->get($resultColumnID);
                if($value != $r->getResult()){
                    $ruleOK = false;
                }
            }

            if($ruleOK) {
                $confidence++;
            }
        }

        $r->setConfidence($confidence / $confidence2);

        return $confidence;

    }

    /**
     * @param Rule $r
     * @return float|int
     */
    public function countSupport(Rule $r){
        $resultColumnName = $this->columnManager->getResultColumnName();

        $resultColumnID = $this->getAttrId($resultColumnName);

        $support = 0;
        for($i = 0; $i < $this->data->count(); $i++){

            $ruleOK = true;
            for($j = 0; $j < $r->getVal()->count(); $j++){

                $colID = $this->getAttrId($r->getVal()->get($j)->getAttribute());

                if($this->columnManager->getNumeric($colID)){

                    $strVal = $this->data->get($i)->get($colID);
                    $value = floatval($strVal);

                    if($r->getVal()->get($j)->getInterval()->check($value) == false){
                        $ruleOK = false;
                        break;
                    }
                }else{

                    $value = $this->data->get($i)->get($colID);
                    if($value != $r->getVal()->get($j)->getValue()){
                        $ruleOK = false;
                        break;
                    }
                }
            }

            if($ruleOK){

                $value = $this->data->get($i)->get($resultColumnID);

                if($value != $r->getResult()){
                    $ruleOK = false;
                }
            }

            if($ruleOK)
                $support++;
        }

        $support = $support / intval($this->data->count());

        $r->setSupport($support);
        return $support;
    }

    /**
     * @param ArrayCollection $intervalList
     */
    public function tmpIntervals(ArrayCollection $intervalList){
        $count = $this->columnManager->getNumberOfColumns();

        for($i = 0; $i < $count; $i++){
            if($this->columnManager->getNumeric($i)){
                $min = 0;
                $max = 0;

                for($j = 0; $j < $this->data->count(); $j++){
                    $strVal = $this->data->get($j)->get($i);
                    $value = floatval($strVal);

                    if($j == 0){
                        $min = $value;
                        $max = $value;
                    }

                    if($value > $max)
                        $max = $value;

                    if($value < $min)
                        $min = $value;
                }

                $numberOfIntervals = 5;

                for($j = 0; $j < $intervalList->count(); $j++){
                    if($intervalList->get($j)->getAttributeName() == $this->columnManager->getName($i)){
                        $numberOfIntervals = $intervalList->get($j)->getNumberOfIntervals();
                        break;
                    }
                }

                $this->columnManager->buildIntervals($i, $min, $max, $numberOfIntervals);
            }
        }
    }

    /**
     * @return int
     */
    public function getNumerOfNumeric(){
        $count = $this->columnManager->getNumberOfColumns();
        $numCount = 0;

        for($i = 0; $i < $count; $i++){
           if($this->columnManager->getNumeric($i)){
               $numCount++;
           }
        }

        return $numCount;
    }

    /**
     * @param int $numId
     * @return string
     */
    public function getNumerName(int $numId){
        $count = $this->columnManager->getNumberOfColumns();
        $numCount = 0;

        for($i = 0; $i < $count; $i++){
            if($this->columnManager->getNumeric($i)){
                $numCount++;
                if($numCount==$numId){
                    return $this->columnManager->getName($i);
                }
            }
        }

        return "";
    }

    /**
     * @param int $id
     * @return bool
     */
    public function getNumeric(int $id){
        return $this->columnManager->getNumeric($id);
    }

    /**
     * @param $name
     * @param $arguments
     * @return string
     */
    public function __call($name, $arguments)
    {
        switch ($name){
            case 'getOriginName':
                if(is_numeric($arguments[0]))
                    return $this->getOriginNameById($arguments[0]);
                else
                    return $this->getOriginNameByInterialName($arguments[0]);
                break;
                break;
        }
    }

    /**
     * @param int $id
     * @return string
     */
    private function getOriginNameById(int $id){
        return $this->columnManager->getOriginName($id);
    }

    /**
     * @param string $name
     * @return string
     */
    private function getOriginNameByInterialName(string $name = null){
        return $this->columnManager->getOriginName($name);
    }


    private function removeColumn(DataSetY $dataSet, $column){

        $column = $dataSet->getAttrId($column);

        $newArray = new ArrayCollection();

        foreach ($dataSet->getHeaders() as $key => $header){
            if($key != $column)
                $newArray->add($header);
        }
        $dataSet->setHeaders($newArray);

        $newArray = new ArrayCollection();
        /** @var ArrayCollection $element */
        foreach ($dataSet->getData() as $element) {
            $newRow = new ArrayCollection();
            foreach ($element as $key => $value){
                if($key != $column){
                    $newRow->add($value);
                }
            }
            $newArray->add($newRow);
        }
        $dataSet->setData($newArray);
        return $dataSet;

    }

    private function removeRowsWithValue(DataSetY $dataSet, $column,  $value){
        $newArray = new ArrayCollection();

        $index = $this->getAttrId($column);
        foreach($dataSet->getData() as $key => $data){
            if($data->get($index) == $value)
                $newArray->add($data);
        }
        $dataSet->setData($newArray);

        return $dataSet;
    }

    public function getDistinctValuesToSelect(){
        $list = new ArrayCollection();
        for($j = 0; $j < $this->getNumberOfAttr(); $j++){
            $textBox = new TextBox();
            $textBox->setTag($this->getAttrName($j));
            if(!$this->getNumeric($j)) {
                for ($i = 0; $i < $this->data->count(); $i++) {

                    if (!$textBox->getMultipleValue()->contains($this->data->get($i)->get($j)))
                        $textBox->getMultipleValue()->add($this->data->get($i)->get($j));

                }
            }else{
                $textBox->setIsNumeric(true);
            }
            $list->add($textBox);
        }
        return $list;
    }
}
