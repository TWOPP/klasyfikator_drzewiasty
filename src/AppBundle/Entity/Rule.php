<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Rule
 */
class Rule
{
    /** @var string  */
    private $result = "";

    /** @var ArrayCollection  */
    private $val;

    /** @var  double */
    private $support = 0;

    /** @var  double */
    private $confidence = 0;

    public function __construct(){
        $this->val = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getVal(): ArrayCollection
    {
        return $this->val;
    }

    /**
     * @param ArrayCollection $val
     */
    public function setVal(ArrayCollection $val)
    {
        $this->val = $val;
    }

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @param string $result
     */
    public function setResult(string $result)
    {
        $this->result = $result;
    }

    /**
     * @return float
     */
    public function getSupport(): float
    {
        return $this->support;
    }

    /**
     * @param float $support
     */
    public function setSupport(float $support)
    {
        $this->support = $support;
    }

    /**
     * @return float
     */
    public function getConfidence(): float
    {
        return $this->confidence;
    }

    /**
     * @param float $confidence
     */
    public function setConfidence(float $confidence)
    {
        $this->confidence = $confidence;
    }




    public function __toString(){
        try {
            $s = "(";
            for($i = 0; $i < $this->val->count(); $i++){

                if($i > 0){
                    $s = $s . " and ";
                }

                if($this->val->get($i)->isNumeric()){
                    $s = $s . $this->val->get($i)->getAttribute() . ' in ' . $this->val->get($i)->getInterval();
                }else{
                    $s = $s . $this->val->get($i)->getAttribute() . ' = ' . $this->val->get($i)->getValue();
                }
            }

            $s = $s . ") -> " . $this->result;

            $s = $s . "  S = " . $this->support . "  C = " . $this->confidence;



            return (string)trim($s);
        } catch (Exception $exception) {
            return '';
        }

    }

    public function getCopy(){
        $rule = new Rule();

        for($i = 0; $i < $this->val->count(); $i++){
            $rule->getVal()->add($this->val->get($i));
        }

        return $rule;
    }

    /**
     * @param CheckData $cd
     * @return bool
     */
    public function check(CheckData $cd){
        $result = true;

        for($i = 0; $i < $this->val->count(); $i++){
            $attr = $this->val->get($i)->getAttribute();
            $value = $this->val->get($i)->getValue();
            $numeric = $this->val->get($i)->isNumeric();
            /** @var Interval $interval */
            $interval = $this->val->get($i)->getInterval();

            for($j = 0; $j < $cd->getAttributes()->count(); $j++){

                if($attr == $cd->getAttributes()->get($j)->getAttribute()){
                    if($numeric){

                        if($interval->check($cd->getAttributes()->get($j)->getRvalue()) == false)
                            $result = false;
                    }else{

                        if($value != $cd->getAttributes()->get($j)->getValue())
                            $result = false;
                    }
                    break;
                }

                if(!$result)
                    break;
            }

        }

        return $result;
    }


}
