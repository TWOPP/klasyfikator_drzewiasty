<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ColumnY
 */
class ColumnY
{
    /** @var string  */
    public $originName = '';

    /** @var string  */
    public $interialName = '';

    /** @var  integer */
    public $id;

    /** @var  bool */
    public $isNumeric;

    /** @var  bool */
    public $result;

    /** @var  ArrayCollection */
    public $intervals;

    /**
     * @return string
     */
    public function getOriginName(): string
    {
        return $this->originName;
    }

    /**
     * @param string $originName
     */
    public function setOriginName(string $originName)
    {
        $this->originName = $originName;
    }

    /**
     * @return string
     */
    public function getInterialName(): string
    {
        return $this->interialName;
    }

    /**
     * @param string $interialName
     */
    public function setInterialName(string $interialName)
    {
        $this->interialName = $interialName;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isNumeric(): bool
    {
        return $this->isNumeric;
    }

    /**
     * @param bool $isNumeric
     */
    public function setIsNumeric(bool $isNumeric)
    {
        $this->isNumeric = $isNumeric;
    }

    /**
     * @return bool
     */
    public function isResult(): bool
    {
        return $this->result;
    }

    /**
     * @param bool $result
     */
    public function setResult(bool $result)
    {
        $this->result = $result;
    }

    /**
     * @return ArrayCollection
     */
    public function getIntervals()
    {
        return $this->intervals;
    }

    /**
     * @param ArrayCollection $intervals
     */
    public function setIntervals(ArrayCollection $intervals = null)
    {
        $this->intervals = $intervals;
    }
}
