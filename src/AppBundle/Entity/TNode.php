<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TNode
 */
class TNode
{
    /** @var ArrayCollection */
    private $children;

    /** @var Interval */
    private $interval;

    /** @var bool */
    private $isNumeric = false;

    /** @var  TNode */
    private $parent;

    /** @var string */
    private $result = '';

    /** @var  string */
    private $attribute = '';

    /** @var string  */
    private $attributeDisplayName = '';

    /** @var string  */
    private $attributeIN = '';

    /** @var string  */
    private $attributeInValue = '';

    /** @var  double */
    private $h;

    /** @var  double */
    private $hw;

    /** @var  string */
    private $inValue;

    /** @var  string */
    private $name;

    /** @var  string */
    private $info;

    /** ##########
     *  GETTERS AND SETTERS
     * ###########
     */


    /**
     * @return ArrayCollection
     */
    public function getChildren(): ArrayCollection
    {
        return $this->children;
    }

    /**
     * @param ArrayCollection $children
     */
    public function setChildren(ArrayCollection $children)
    {
        $this->children = $children;
    }

    /**
     * @param TNode $TNode
     */
    public function addChildren(TNode $TNode){
        $this->children->add($TNode);
    }

    /**
     * @return Interval
     */
    public function getInterval(): Interval
    {
        return $this->interval;
    }

    /**
     * @param Interval $interval
     */
    public function setInterval(Interval $interval)
    {
        $this->interval = $interval;
    }

    /**
     * @return bool
     */
    public function isIsNumeric(): bool
    {
        return $this->isNumeric;
    }

    /**
     * @param bool $isNumeric
     */
    public function setIsNumeric(bool $isNumeric)
    {
        $this->isNumeric = $isNumeric;
    }

    /**
     * @return TNode
     */
    public function getParent(): TNode
    {
        return $this->parent;
    }

    /**
     * @param TNode $parent
     */
    public function setParent(TNode $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @param string $result
     */
    public function setResult(string $result)
    {
        $this->result = $result;
    }

    /**
     * @return string
     */
    public function getAttribute(): string
    {
        return $this->attribute;
    }

    /**
     * @param string $attribute
     */
    public function setAttribute(string $attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * @return string
     */
    public function getAttributeDisplayName(): string
    {
        return $this->attributeDisplayName;
    }

    /**
     * @param string $attributeDisplayName
     */
    public function setAttributeDisplayName(string $attributeDisplayName)
    {
        $this->attributeDisplayName = $attributeDisplayName;
    }

    /**
     * @return string
     */
    public function getAttributeIN(): string
    {
        return $this->attributeIN;
    }

    /**
     * @param string $attributeIN
     */
    public function setAttributeIN(string $attributeIN)
    {
        $this->attributeIN = $attributeIN;
    }

    /**
     * @return string
     */
    public function getAttributeInValue(): string
    {
        return $this->attributeInValue;
    }

    /**
     * @param string $attributeInValue
     */
    public function setAttributeInValue(string $attributeInValue)
    {
        $this->attributeInValue = $attributeInValue;
    }

    /**
     * @return float
     */
    public function getH()
    {
        return $this->h;
    }

    /**
     * @param float $h
     */
    public function setH(float $h)
    {
        $this->h = $h;
    }

    /**
     * @return float
     */
    public function getHw()
    {
        return $this->hw;
    }

    /**
     * @param float $hw
     */
    public function setHw(float $hw)
    {
        $this->hw = $hw;
    }

    /**
     * @return string
     */
    public function getInValue(): string
    {
        return $this->inValue;
    }

    /**
     * @param string $inValue
     */
    public function setInValue(string $inValue)
    {
        $this->inValue = $inValue;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getInfo(): string
    {
        return $this->info;
    }

    /**
     * @param string $info
     */
    public function setInfo(string $info)
    {
        $this->info = $info;
    }

    /** ##########
     *  FUNCTIONS
     * ###########
     */

    const COUNTER_METHOD = 'strlen';

    /**
     * TNode constructor.
     * @param string $attributeIn
     * @param string $attributeInValue
     * @param string $attribute
     * @param string $attributeDisplayName
     * @param string $result
     * @param string $info
     * @param bool $isNumeric
     * @param Interval $interval
     */
    public function __construct(string $attributeIn = null, string $attributeInValue = null, string $attribute = null, string $attributeDisplayName = null, string $result = null, string $info = null, bool $isNumeric = null, Interval $interval = null){

        $this->result = $result;
        $this->attribute = $attribute;
        $this->attributeIN = $attributeIn;
        $this->attributeInValue = $attributeInValue;
        $this->info = $info;
        $this->attributeDisplayName = $attributeDisplayName;
        $this->isNumeric = $isNumeric;
        $this->interval = $interval;
        $this->children = new ArrayCollection();

        $this->name = '';

        if($this->isNumeric)
            $this->name .= ' '. $interval . '  ';
        else {

            if (strlen($attributeInValue) > 0)
                $this->name .= '(' . $attributeInValue . ') ';
        }

//        $this->name .= $this->attributeDisplayName;

//        if(strlen($this->result) > 0)
//            $this->name .= ' : ' . $this->result;

    }

    /**
     * @param string $attributeIn
     * @param string $attributeInValue
     * @param string $attribute
     * @param string $attributeDisplayName
     * @param string $result
     * @param string $info
     * @param TNode $parent
     * @param bool $isNumeric
     * @param Interval $interval
     * @return TNode
     */
    public static function createNode(string $attributeIn = null, string $attributeInValue = null, string $attribute = null, string $attributeDisplayName = null, string $result = null, string $info = null, TNode $parent = null, bool $isNumeric = null, Interval $interval = null){

        $node = new TNode($attributeIn, $attributeInValue, $attribute, $attributeDisplayName, $result, $info, $isNumeric, $interval);
        $node->setParent($parent);

        if($parent != null)
            $parent->addChildren($node);

        return $node;
    }

    /**
     * @return string
     */
    public function getEntrString(){
        return 'H = ' . $this->h . ' Hw= ' . $this->hw . ' IG= ' . ($this->h - $this->hw);
    }

    /**
     * @param CheckData $data
     */
    public function check(CheckData $data){

        if(strlen($this->result) > 0) {
            $data->setResult($this->result);
        }else{

            if(strlen($this->attribute) == 0){

                if($this->children->count() > 0){
                    $this->children->get(0)->check($data);
                }
            }
            else{

                $value = '';
                $dvalue = 0;
                $numeric = false;

                for($i = 0; $i < $data->getAttributes()->count(); $i++){

                    if($data->getAttributes()->get($i)->getAttribute() == $this->attribute){

                        if($data->getAttributes()->get($i)->isNumeric()){

                            $dvalue = $data->getAttributes()->get($i)->getRvalue();
                            $numeric = true;
                        }else{

                            $value = $data->getAttributes()->get($i)->getValue();
                        }
                        break;
                    }
                }

                for($i = 0; $i < $this->children->count(); $i++){

                    if($this->isNumeric){

                        if($this->children->get($i)->getInterval()->check($dvalue)){

                            $this->children->get($i)->check($data);
                            break;
                        }
                    }else{

                        if($value == $this->children->get($i)->getAttributeInValue()){
                            $this->children->get($i)->check($data);
                            break;
                        }
                    }
                }
            }
        }
    }



}
