<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TextBox
 */
class TextBox
{
    /** @var string  */
    private $text = "";
    /** @var string  */
    private $tag = "";
    /** @var bool  */
    private $visible = false;
    /** @var  ArrayCollection */
    private $multipleValue;
    /** @var bool  */
    private $isNumeric = false;
    /** @var string  */
    private $checked = "";

    public function __construct(){
        $this->multipleValue = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getChecked(): string
    {
        return $this->checked;
    }

    /**
     * @param string $checked
     */
    public function setChecked(string $checked)
    {
        $this->checked = $checked;
    }

    /**
     * @return bool
     */
    public function isNumeric(): bool
    {
        return $this->isNumeric;
    }

    /**
     * @param bool $isNumeric
     */
    public function setIsNumeric(bool $isNumeric)
    {
        $this->isNumeric = $isNumeric;
    }

    /**
     * @return ArrayCollection
     */
    public function getMultipleValue(): ArrayCollection
    {
        return $this->multipleValue;
    }

    /**
     * @param ArrayCollection $multipleValue
     */
    public function setMultipleValue(ArrayCollection $multipleValue)
    {
        $this->multipleValue = $multipleValue;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     */
    public function setTag(string $tag)
    {
        $this->tag = $tag;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }

    /**
     * @param bool $visible
     */
    public function setVisible(bool $visible)
    {
        $this->visible = $visible;
    }



}
