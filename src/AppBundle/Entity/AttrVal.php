<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AttrVal
 */
class AttrVal
{
    /** @var string  */
    private $attribute = "";
    /** @var string  */
    private $value = "";
    /** @var int  */
    private $rvalue = 0;
    /** @var bool  */
    private $isNumeric = false;
    /** @var Interval */
    private $interval = null;

    public function __construct(){
        $this->interval = new Interval();
    }

    /**
     * @return string
     */
    public function getAttribute(): string
    {
        return $this->attribute;
    }

    /**
     * @param string $attribute
     */
    public function setAttribute(string $attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getRvalue(): int
    {
        return $this->rvalue;
    }

    /**
     * @param int $rvalue
     */
    public function setRvalue(int $rvalue)
    {
        $this->rvalue = $rvalue;
    }

    /**
     * @return bool
     */
    public function isNumeric(): bool
    {
        return $this->isNumeric;
    }

    /**
     * @param bool $isNumeric
     */
    public function setIsNumeric(bool $isNumeric)
    {
        $this->isNumeric = $isNumeric;
    }

    /**
     * @return Interval
     */
    public function getInterval(): Interval
    {
        return $this->interval;
    }

    /**
     * @param Interval $interval
     */
    public function setInterval(Interval $interval)
    {
        $this->interval = $interval;
    }



}
