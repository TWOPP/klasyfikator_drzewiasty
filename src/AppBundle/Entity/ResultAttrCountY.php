<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ResultAttrCountY
 */
class ResultAttrCountY
{
    /** @var string  */
    private $name = "";
    /** @var Interval */
    private $interval = null;
    /** @var bool  */
    private $isNumeric = false;
    /** @var int  */
    private $count = 0;
    /** @var ArrayCollection  */
    private $list ;

    public function __construct(){
        $this->list = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name = null)
    {
        $this->name = $name;
    }

    /**
     * @return Interval|null
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * @param Interval $interval
     */
    public function setInterval(Interval $interval)
    {
        $this->interval = $interval;
    }

    /**
     * @return bool
     */
    public function isNumeric(): bool
    {
        return $this->isNumeric;
    }

    /**
     * @param bool $isNumeric
     */
    public function setIsNumeric(bool $isNumeric)
    {
        $this->isNumeric = $isNumeric;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count)
    {
        $this->count = $count;
    }

    /**
     * @return ArrayCollection
     */
    public function getList(): ArrayCollection
    {
        return $this->list;
    }

    /**
     * @param ArrayCollection $list
     */
    public function setList(ArrayCollection $list)
    {
        $this->list = $list;
    }

    /**
     * @return int
     */
    public function getSum(){
        $sum = 0;
        for($i = 0; $i < $this->list->count(); $i++){
            $sum = $sum + $this->list->get($i)->getCount();
        }

        return $sum;
    }

    /**
     * @param string $value
     */
    public function addValue(string $value){
        $found = false;

        for($i = 0; $i < $this->list->count(); $i++){
            if($this->list->get($i)->getName() == $value){
                $this->list->get($i)->setCount($this->list->get($i)->getCount() + 1);
                $found = true;
                break;
            }
        }

        if($found == false){
            $rc = new ResultCount();
            $rc->setName($value);
            $rc->setCount(1);
            $this->list->add($rc);
        }
    }


}
